sofa_perfs
----------

This repository contains a documentation on performances optimization in SOFA.


[Slides](slides.md) have been made with [reveal-md](https://github.com/webpro/reveal-md), a Markdown layer over [reveal.js](https://revealjs.com/).  
Serve slides locally with:
```shell
reveal-md slides.md
```

To install reveal-md:

```shell
npm install -g reveal-md
```