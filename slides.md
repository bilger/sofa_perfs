---
title: Improve the Performances of your SOFA Simulation
theme: simple
highlightTheme: "obsidian"
revealOptions:
  navigationMode: 'linear'
  width: 1920
  height: 1080
  slideNumber: 'c/t'
  transition: 'slide'
---

## Improve the Performances of your SOFA Simulation

_Alexandre Bilger, DEFROST, Inria Lille_

SOFA Week Symposium, 2023

<div id="three-col1-center">

![SOFA](https://www.sofa-framework.org/wp-content/uploads/2013/01/SOFA_LOGO_ORANGE_2-normal-300x86.png)

</div>

<div id="three-col1-center">

![SOFA](images/inr_logo_rouge.svg) <!-- .element width="50%" -->

</div>


<div id="three-col3-center">

![defrost](images/LOGO_DEFROST.svg) <!-- .element width="50%" -->

</div>



---

#### Your SOFA simulation

![](https://media.giphy.com/media/l2JHVUriDGEtWOx0c/giphy.gif) <!-- .element width="100%" -->

----

#### Your dreamed SOFA simulation

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExZHpocWMwanNhdTFrdjFob202bXZuMWkzbjdjem9zczI2Y2VpbGtybCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/tBemJSc7bAzc2UIq7y/giphy.gif) <!-- .element width="80%" -->

----

#### Improve the performances

![](https://media.giphy.com/media/bTMzzPaOABxzG/giphy.gif) <!-- .element width="60%" -->

---

## Find the Bottleneck

----

## Recommendations

- Don't optimize early
- Use profiling tools to determine where the bottleneck is
    - (don't assume)

----

## Profiling Tools

Two profiling tools are available:

- SOFA timers
- Tracy profiler

----

## SOFA Timers

![Profiler in GLFW UI](images/profiler_glfw.gif) <!-- .element width="60%" -->

```C++
SCOPED_TIMER("FreeMotion");
simulation::SolveVisitor freeMotion(params, dt, true, d_parallelODESolving.getValue());
node->execute(&freeMotion);
```

----

## Tracy

![Tracy profiler](images/tracy.gif) <!-- .element width="60%" -->

[https://github.com/wolfpld/tracy](https://github.com/wolfpld/tracy)

<sup>
A real time, nanosecond resolution, remote telemetry, hybrid frame and sampling profiler for games and other applications.
</sup>

---

## Reduce the Complexity

<div id="left">

Reduce number of vertices/elements

![](https://cdn.comsol.com/cyclopedia/mesh-refinement/image5.jpg)

</div>

<div id="right">

Use multiple models using mappings

![](images/multimodels.png) <!-- .element width="80%" -->

</div>


---

## Free Motion Animation Loop

```python[1-9|2-4|6-8|1-9]
root.addObject('FreeMotionAnimationLoop',
  # If true, executes free motion step 
  # and collision detection step in parallel
  parallelCollisionDetectionAndFreeMotion = True, 

  # If true, solves all the ODEs in parallel 
  # during the free motion step
  parallelODESolving = True                       
)
```

---

## Linear Solver

### Types of Linear Solvers

| Name            | Template       |             |
|----------------:|:--------------:|:------------|
| SparseLDLSolver | CRS            | Assembled   |
| SparseLDLSolver | CRS3x3         | Assembled   |
| CGLinearSolver  | CRS            | Assembled   |
| CGLinearSolver  | CRS3x3         | Assembled   |
| CGLinearSolver  | GraphScattered | Matrix-free |


\
CRS = `CompressedRowSparseMatrix`

CRS3x3 = `CompressedRowSparseMatrixMat3x3`

----

## Conjugate Gradient

<div id="left">

![](images/cg.svg) <!-- .element width="60%" -->

</div>

\
Iterative algorithm allowing to configure a maximum number of iterations: 

Balance between accuracy and performances

Also valid for the Gauss-Seidel algorithm in constraint solvers

Consider also `ShewchukPCGLinearSolver`, a preconditioned conjugate gradient

----


## Asynchronous LDL Solver

The factorization of the matrix is performed in a different thread in order to speed up the simulation.


\
Replace
```python
root.addObject('SparseLDLSolver', ...)
```

by

```python
root.addObject('AsyncSparseLDLSolver', ...)
```


----


## Constant Sparsity Pattern

![](images/matrixAssembly.svg) <!-- .element width="60%" -->


----

## Block Tridiagonal Matrix


![BTD](images/btd.svg) <!-- .element width="45%" -->

- Adapted for linear objects (beams)
- Linear solver: BTDLinearSolver
  - Template: BTDMatrix

----

## Constant Insertion Order


If the insertion order is constant, a mapping between insertion location and compressed format allows to bypass the insertion and compression.

Replace

```xml
<MatrixLinearSystem template="CompressedRowSparseMatrixd" name="A"/>
<EigenSimplicialLDLT template="CompressedRowSparseMatrixd" linearSystem="@A"/>
```

by

```xml
<ConstantSparsityPatternSystem template="CompressedRowSparseMatrixd" name="A"/>
<EigenSimplicialLDLT template="CompressedRowSparseMatrixd" linearSystem="@A"/>
```

----

## Other Optimizations on Linear Solvers

```python[1-5|3-4]
node.addObject('EigenSimplicialLDLT',
  template='CompressedRowSparseMatrixMat3x3',
  # Ordering method: "Natural", "AMD", "COLAMD", "Metis"
  ordering='AMD'
)
```

```python[1-8|3-5]
node.addObject('MatrixLinearSystem',
  template='CompressedRowSparseMatrixMat3x3', name='A',
  # If true, independent matrices (global matrix vs mapped matrices)
  # are assembled in parallel
  parallelAssemblyIndependentMatrices = True
)
node.addObject('EigenSimplicialLDLT',
  template='CompressedRowSparseMatrixMat3x3', linearSystem='@A')
```
<!-- .element class="fragment fade-up" -->

---

## Constraint Solving

<div id="left">

Assembled or matrix-free:

```python
root.addObject('GenericConstraintSolver',
  # Method used to solve the constraint problem,
  # among: 
  # - "ProjectedGaussSeidel"
  # - "UnbuiltGaussSeidel"
  # - "NonsmoothNonlinearConjugateGradient"
  resolutionMethod = 'UnbuiltGaussSeidel'
)
```

or

```python
root.addObject('LCPConstraintSolver',
  # LCP is not fully built to increase 
  # performances in some cases
  build_lcp = False
)
```

</div>

<div id="right" class="fragment fade-left">

Leverage parallelism:

```python
node.addObject('EigenSimplicialLDLT',
  template='CompressedRowSparseMatrixMat3x3',
  # Parallelize the computation of the product
  # $J*M^{-1}*J^T$ where $M$ is the matrix 
  # of the linear system and $J$ is any matrix 
  # with compatible dimensions 
  # (involved in constraint solving, to compute
  # the compliance matrix projected in the
  # constraints space)
  parallelInverseProduct = True
)
```

```python
root.addObject('GenericConstraintSolver',
  # Build constraints matrix concurrently
  multithreading = True)
```

</div>

---

## Parallel Components

In MultiThreading plugin

| Sequential               | Parallel                         |
|-------------------------:|:---------------------------------|
| BruteForceBroadPhase     | ParallelBruteForceBroadPhase     |
| BVHNarrowPhase           | ParallelBVHNarrowPhase           |
| TetrahedronFEMForceField | ParallelTetrahedronFEMForceField |
| HexahedronFEMForceField  | ParallelHexahedronFEMForceField  |
| StiffSpringForceField    | ParallelStiffSpringForceField    |
| MeshSpringForceField     | ParallelMeshSpringForceField     |
| CGLinearSolver           | ParallelCGLinearSolver           |


---

## Model Order Reduction

> Reduce the size and the online computation time of a Finite Element Model

> Expensive offline simulations based on the whole model, we apply snapshot-proper orthogonal decomposition to sharply reduce the number of state variables model

[https://github.com/SofaDefrost/ModelOrderReduction](https://github.com/SofaDefrost/ModelOrderReduction)

![MOR](images/mor.png) <!-- .element width="30%" -->


---

## GPU Computing using CUDA

<div id="left">

Plugins:

- SofaCUDA (included with SOFA sources)
  - Subset of SOFA components
  - Templates (CudaVec3, CudaRigid3, etc)

```xml
<MechanicalObject template="CudaVec3f"/>
```

- [SofaCUDALinearSolver](https://github.com/SofaDefrost/SofaCUDALinearSolver)
  - `CUDASparseCholeskySolver`

</div>


<div id="right">

![GPU perfs](images/gpu_perfs.png) <!-- .element width="80%" -->

</div>

---

## Compilation Options

- `SOFA_ENABLE_FAST_MATH`
- `SOFA_ENABLE_SIMD`
- `SOFA_ENABLE_LINK_TIME_OPTIMIZATION`

---

## The Doc

[https://www.sofa-framework.org/community/doc/using-sofa/performances/improve-performances/](https://www.sofa-framework.org/community/doc/using-sofa/performances/improve-performances/)

---

## The End

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExZHpocWMwanNhdTFrdjFob202bXZuMWkzbjdjem9zczI2Y2VpbGtybCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/tBemJSc7bAzc2UIq7y/giphy.gif) <!-- .element width="80%" class="fragment fade-out" -->

URL: [https://bilger.gitlabpages.inria.fr/sofa_perfs](https://bilger.gitlabpages.inria.fr/sofa_perfs)

The sources: [https://gitlab.inria.fr/bilger/sofa_perfs](https://gitlab.inria.fr/bilger/sofa_perfs)